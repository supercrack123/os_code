#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>

typedef struct position{ // 입력되는 점들을 받기 위한 struct
    float x;
    float y;
    int groupings;
} pos;

typedef struct aver{ // 입력되는 점들의 clustering에 대한 평균 값을 계산하기 위한 struct
    float x;
    float y;
} average;

long timecheck(){ // microsecond 단위의 연산 수행 시간을 측정하기위해 사용되는 시간 측정 구조체
    struct timeval current;
    long microcurrent;
    gettimeofday(&current, 0);
    microcurrent = current.tv_usec + current.tv_sec*1000000; // 마이크로초 단위로 시간 계산
    return microcurrent;
}

float differ_distance(float avg_x, float avg_y, float temp_x, float temp_y ) //해당 점들과 각 클러스터의 평균값 간의 차이를 구하기 위한 함수
{
    return fabs(avg_x - temp_x) + fabs(avg_y - temp_y);
}

void get_cluster(int num_point, int clusters, average * averages, pos * points) // 각 클러스터의 평균값 비교를 통해 해당 점들의 클러스터 지정 함수
{
    float differ_min; // differ_distance 함수에 따르는 해당 점과 평균 값간의 차이를 대입할 변수
    float min = FLT_MAX; // clustering을 위해 가장 적은 차이를 저장하는 변수
    
    for(int a = 0; a < num_point; a++) // 모든 점들에 대해
    {
        for(int b = 0; b < clusters; b++) // 각 클러스터의
        {
            differ_min = differ_distance(averages[b].x, averages[b].y, points[a].x, points[a].y); // 평균값과 점들의 차이를 계산하여
            if(b == 0) {min = differ_min; points[a].groupings = 0;} // 처음 클러스터 초기화
            
            
            if(differ_min < min) // 가장 작은 차이를 갖는 값들을
            {
                min = differ_min; // 저장하고
                points[a].groupings = b; // 클러스터를 지정해줌.
            }
        }
    }
}

int update_mean(int num_point, int cluster, pos * points ,float * average_x, float * average_y) // 각 점들이 클러스터링 되고, 각 클러스터링 마다 평균 값을 갱신하기 위한 함수
{
    int count = 0; // 각 클러스터의 전체 변수를 측정하기 위한 변수
    float total_differ_x = 0.0, total_differ_y = 0.0; // 각 클러스터의 모든 x, y 값들을 합산하고 평균을 구하기 위한 편수
    float mean_x = 0, mean_y = 0; // 각 클러스터의 x, y 평균 변수
    
    for(int i = 0; i < num_point; i++) // 모든 점들에서 각 클러스터에 해당하는 값들 추출후 더해줌.
    {
        if(points[i].groupings == cluster ) {count++; total_differ_x += points[i].x; total_differ_y += points[i].y; }
    }
    
    if(count == 0)
        return 1;
    
    
    mean_x = total_differ_x / count ;
    mean_y = total_differ_y / count ;
    *average_x = mean_x;
    *average_y = mean_y;
    return 0;
}

int check_means(average * ex_mean, average * current_mean, int clusters) // 이전의 클러스터의 평균값들과 갱신된 클러스터의 평균값들의 비교를 통해 알고리즘의 반복절차를 끝낼 수렴을 결정지을 함수
{
    int flag = 1;
    for(int i = 0; i < clusters ; i++) // 모든 클러스터 값들에 대해
    {
        if((ex_mean[i].x != current_mean[i].x) || (ex_mean[i].y != current_mean[i].y)) // 갱신된 평균값이 이전의 평균값과 같은지 확인
        {
            ex_mean[i].x = current_mean[i].x;
            ex_mean[i].y = current_mean[i].y;
            flag = 0;
        }
    }
    return flag;
}

void initialize_ex(average * ex_average, average * averages, int clusters) // check_means 함수에서 이전의 평균갑과 현재의 평균값의 비교에 앞서 비교 대상의 구조체 값을 초기화 시켜주는 함수
{
    for(int i = 0; i< clusters ; i++)
    {ex_average[i].x = averages[i].x; ex_average[i].y = averages[i].y;}
}

void K_means(int iter, int num_point, int * clusters, pos *points, average *averages){ // 반복횟수, 점의 숫자, 클러스터, 데이터 저장 구조체 변수들
    
    int flag; // check_means에 사용될 함수
    average ex_average[*clusters]; // 이전의 평균값들을 저장할 구조체
    
    initialize_ex(ex_average,averages,*clusters);
    
    for(int i = 0; i < iter; i++) // 반복
    {
        get_cluster(num_point, *clusters, averages, points);
        
        for(int update=0;update < *clusters ; update++) // 각 클러스터마다 평균값 업데이트
        {
            
            int checking_zero = update_mean(num_point, update, points , &averages[update].x, &averages[update].y);
            
            if (checking_zero == 1) {
                float temp1 = averages[update].x; float temp2 = averages[update].y;
                averages[update].x = averages[*clusters].x; averages[update].y = averages[*clusters].y;
                int temp = *clusters;
                temp--;
                *clusters = temp;
                update --;
            }
        }
        
        
        flag = check_means(ex_average, averages, *clusters); // 이전의 평균과 갱신된 평균값 비교 후, flag 변수를 통해 수렴 여부 확인
        if(flag)break;
        
    }
}

int main()
{
    int test_case, iteration, num_cluster, num_point;
    // test_case 수, 반복횟수, 입력받은 클러스터의 수, 점들의 수
    long start, finish, spent;
    // 전체 수행시간 측정을 위한 시작, 완료, 순수하게 걸린 시간을 위한 변수
    
    scanf("%d",&test_case);
    for(int i = 0; i< test_case ; i++)
    {
        start = timecheck(); // 순수 연산 시간 측정 시작
        
        scanf("%d",&iteration);
        scanf("%d",&num_cluster);
        scanf("%d",&num_point);
        pos data[num_point];
        average cluster_average[num_cluster];
        
        for(int j = 0; j< num_point ; j++) // 구조체 변수에, 초기 클러스터 평균값과 점의 데이터 입력
        {
            scanf("%f %f", &data[j].x, &data[j].y);
            if(j < num_cluster)
            {
                cluster_average[j].x = data[j].x; cluster_average[j].y = data[j].y;
                data[j].groupings = j;
            }
            
        }
        
        K_means(iteration, num_point, & num_cluster, data, cluster_average); //K-Means Clustering Algorithm
        
        finish = timecheck(); // 순수 연산 시간 측정 종료
        spent = finish - start; // 순수 연산 시간 계산
        
        printf("Test Case #%d\n",i); //test_case 수 출력
        printf("%ld microseconds\n",spent); // 연산 시간 출력
        for(int k = 0; k < num_point ; k++) // 각 test_case에 따른 각 점들의 clustering 출력
            printf("%d\n",data[k].groupings);
    }
    
    return 0;
}


