#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <unistd.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>


typedef struct position{ // 입력되는 점들을 받기 위한 struct
    float x;
    float y;
    int groupings;
} pos;

typedef struct aver{ // 입력되는 점들의 clustering에 대한 평균 값을 계산하기 위한 struct
    float x;
    float y;
} average;

// shared_memory 사용을 위한 전역변수 선언입니다.
int datas = 0, cluster_averages = 0; // 점의 수, 클러스터 수에 따른 평균의 수, shared_memory 인자 사용에 활용될 변수입니다.
average * cluster_average; // 각 클러스터의 평균을 저장하기 위한 구조체 변수입니다.
pos * data; // 점의 데이터를 저장하기 위한 변수입니다.

void sig_handler(int signo)
{
    int ab = 0;
}

long timecheck(){ // microsecond 단위의 연산 수행 시간을 측정하기위해 사용되는 시간 측정 구조체
    struct timeval current;
    long microcurrent;
    gettimeofday(&current, 0);
    microcurrent = current.tv_usec + current.tv_sec*1000000; // 마이크로초 단위로 시간 계산
    return microcurrent;
}

float differ_distance(float avg_x, float avg_y, float temp_x, float temp_y ) //해당 점들과 각 클러스터의 평균값 간의 차이를 구하기 위한 함수
{
    return fabs(avg_x - temp_x) + fabs(avg_y - temp_y);
}

void get_cluster(int num_point, int clusters, average * averages, pos * points, int child_num)// 각 프로세스들이 n/4 씩 데이터를 병렬처리하기위한 함수, 각 클러스터의 평균값 비교를 통해 해당 점들의 클러스터 지정 함수
{
    
    float differ_min; // differ_distance 함수에 따르는 해당 점과 평균 값간의 차이를 대입할 변수
    float min = FLT_MAX; // clustering을 위해 가장 적은 차이를 저장하는 변수
    int workload = num_point / 4 ; // 점의 갯수에 따른 작업량 결정 변수
    int start = workload * child_num; // PROCESS의 number 에 맞는 데이터 처리 시작을 알려주는 변수
    int end; // PROCESS의 데이터 마무리 구간을 알려주는 변수
    
    if(num_point < 4){ // 입력되는 데이터가 4개의 PROCESS보다 적은 4개 미만일 경우, 스레드 하나가 모든 데이터 처리
        start = 0;
        end = num_point;
    }
    else{ // 아닐경우 PROCESS의 number에 따라 해당 작업량을 결정하고 마무리구간 선언
        if(child_num == 3)
            end = num_point;
        else
            end = start + workload;
    }
    
    
    for(int a = start; a < end; a++) // PROCESS의 주어진 구간에 따라 점들을
    {
        for(int b = 0; b < clusters; b++) // 입력받은 클러스터 수만큼
        {
            differ_min = differ_distance(averages[b].x, averages[b].y, points[a].x, points[a].y);
            if(b == 0) {min = differ_min; points[a].groupings = 0;}
            
            
            if(differ_min < min) // 가장 작은 차이를 갖는 값들을
            {
                min = differ_min; // 저장하고
                points[a].groupings = b; // 클러스터를 지정해줌.
            }
        }
    }
    
}

int update_mean(int num_point, int cluster, pos * points ,float * average_x, float * average_y) // 각 점들이 클러스터링 되고, 각 클러스터링 마다 평균 값을 갱신하기 위한 함수
{
    int count = 0; // 각 클러스터의 전체 변수를 측정하기 위한 변수
    float total_differ_x = 0.0, total_differ_y = 0.0; // 각 클러스터의 모든 x, y 값들을 합산하고 평균을 구하기 위한 편수
    float mean_x = 0, mean_y = 0; // 각 클러스터의 x, y 평균 변수
    
    for(int i = 0; i < num_point; i++) // 모든 점들에서 각 클러스터에 해당하는 값들 추출후 더해줌.
    {
        if(points[i].groupings == cluster ) {count++; total_differ_x += points[i].x; total_differ_y += points[i].y; }
    }
    
    if(count == 0)
        return 1;
    
    mean_x = total_differ_x / count ;
    mean_y = total_differ_y / count ;
    *average_x = mean_x;
    *average_y = mean_y;
    return 0;
}

int check_means(average * ex_mean, average * current_mean, int clusters) // 이전의 클러스터의 평균값들과 갱신된 클러스터의 평균값들의 비교를 통해 알고리즘의 반복절차를 끝낼 수렴을 결정지을 함수
{
    int flag = 1;
    for(int i = 0; i < clusters ; i++) // 모든 클러스터 값들에 대해
    {
        if((ex_mean[i].x != current_mean[i].x) || (ex_mean[i].y != current_mean[i].y)) // 갱신된 평균값이 이전의 평균값과 같은지 확인
        {
            ex_mean[i].x = current_mean[i].x;
            ex_mean[i].y = current_mean[i].y;
            flag = 0;
        }
    }
    return flag;
}

void initialize_ex(average * ex_average, average * averages, int clusters) // check_means 함수에서 이전의 평균갑과 현재의 평균값의 비교에 앞서 비교 대상의 구조체 값을 초기화 시켜주는 함수
{
    for(int i = 0; i< clusters ; i++)
    {ex_average[i].x = averages[i].x; ex_average[i].y = averages[i].y;}
}


int main(int argc, char *argv[])
{
    
    int test_case, iteration, num_cluster, num_point; // test_case 수, 반복횟수, 클러스터수, 점의 개수
    int key = 125;
    pid_t child[4]; // 4개의 프로세스 생성을 위한 함수
    int runProcess = 0; // 프로세스 수 관련 변수, 총 4개의 프로세스 생성
    int count = 0;
    int state; // wait 함수 인자 반환을 위한 함수
    long start, finish, spent; // 전체 수행시간 측정을 위한 시작, 완료, 순수하게 걸린 시간을 위한 변수
    //long start2, finish2, spent2; // 프로세스 생성시간 측정을 위한 변수
    
    
    scanf("%d",&test_case);
    for(int i = 0; i< test_case ; i++) //test_case 수만큼 반복
    {
        start = timecheck(); // 전체 성능 시간 측정 시작
        
        runProcess = 0;
        
        scanf("%d",&iteration);
        scanf("%d",&num_cluster);
        scanf("%d",&num_point);
        
        datas = shmget(key,sizeof(pos)*num_point, IPC_CREAT|0666|IPC_EXCL); // 주어진 점의 수만큼 공유메모리 생성
        cluster_averages = shmget(key+1,sizeof(average)*num_cluster, IPC_CREAT|0666|IPC_EXCL); // 주어진 클러스터의 수만큼 공유메모리 생성
        
        if (datas < 0 || cluster_averages < 0) // 공유메모리 생성이 안될 시 오류메세지 출력
        {
            perror("share memory error\n");
            exit(-1);
        }
        
        cluster_average = shmat(cluster_averages,NULL,0); // 클러스터의 평균을 저장하기 위한 구조체 변수 할당
        data = shmat(datas,NULL,0); // 점의 값을 입력 받기 위한 구조체 변수 할당
        
        
        
        for(int j = 0; j< num_point ; j++) // 구조체 변수에, 초기 클러스터 평균값과 점의 데이터 입력
        {
            scanf("%f %f", &data[j].x, &data[j].y);
            data[j].groupings = -1;
            if(j < num_cluster)
            {
                cluster_average[j].x = data[j].x; cluster_average[j].y = data[j].y;
                data[j].groupings = j;
            }
            
        }
        
        int flag; // check_means에 사용될 함수
        average ex_average[num_cluster]; // 이전의 평균값들을 저장할 구조체
        
        initialize_ex(ex_average,cluster_average,num_cluster);
        int flag2 = 0;
        
        for(int i = 0; i < iteration; i++)
        {
            
            while(runProcess < 4) {
                //start2 = timecheck(); // 프로세스 생성시간 측정 시작
                child[runProcess] = fork(); // 프로세스 생성
                //finish2 = timecheck(); // 프로세스 생성시간 측정 완료
                //spent2 = finish2 - start2; // 프로세스 생성시간 계산
                //printf("one process create time is %ld microseconds\n",spent2); // 프로세스 생성시간 출력
                
                if(child[runProcess] < 0) {
                    return -1;
                }
                else if(child[runProcess] == 0) { // 자식 프로세스
                    get_cluster(num_point, num_cluster, cluster_average, data, runProcess);
                    signal(SIGINT, (void *)sig_handler);
                    exit(0);
                } else { //부모 프로세스
                    while(wait(&state)>0); // 자식의 작업이 끝날때까지 대기
                    
                    if(count == 3) // 4개의 프로세스가 작업 수행 완료시
                    {
                        count = 0;
                        for(int update=0; update < num_cluster ; update++) // 각 클러스터의 평균갑 업데이트
                        {
                            int checking_zero = update_mean(num_point, update, data , &cluster_average[update].x, &cluster_average[update].y);
                            if (checking_zero == 1) {
                                cluster_average[update].x = cluster_average[num_cluster].x; cluster_average[update].y = cluster_average[num_cluster].y;
                                num_cluster --;
                                update --;
                            }
                        }
                        
                        flag = check_means(ex_average, cluster_average, num_cluster); // 각 클러스터의 평균값이 이전 값과 동일하면 수렴한 것으로 판단
                        if(flag){flag2 = 1; break;} // flag2 변수를 통해 전체 반복문 탈출
                    }
                    else{ count ++; }
                }
                runProcess++; // 그다음 프로세스 병렬처리에 사용
            }
            runProcess = 0;
            if(flag2 == 1) break;
        }
        finish = timecheck(); // 전체성능 시간 측정 종료
        spent = finish - start; // 순수 성능 시간 계산
        printf("Test Case #%d\n",i); //test_case 수 출력
        printf("%ld microseconds\n",spent); // 연산 시간 출력
        for(int k = 0; k < num_point ; k++) // 각 test_case에 따른 각 점들의 clustering 출력
            printf("%d\n",data[k].groupings);
        
        shmdt(data); // data 변수 공유메모리 해제
        shmdt(cluster_average); // 클러스터평균 변수 공유메모리 해제
        shmctl(datas, IPC_RMID, 0); // 확인 및 제거
        shmctl(cluster_averages, IPC_RMID, 0); // 확인 및 제거
    }
    
    return 0;
}
